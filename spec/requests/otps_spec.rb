require 'rails_helper'

RSpec.describe "Otps", type: :request do
  describe "POST /generate_otp" do
    it "creates OTP" do
      post "/generate_otp"
      expect(response).to have_http_status(:created)

      json_response = JSON.parse(response.body)
      expect(json_response).to include("otp", "expires_at")

      otp = json_response["otp"]
      expires_at = json_response["expires_at"]
      expect(otp).to be_present
      expect(expires_at).to be_present
    end
  end

  describe "POST /verify_otp" do
    it "verifies valid OTP" do
      otp_record = Otp.create(otp: 123456, expires_at: Time.now + 3.minutes)

      post "/verify_otp", params: {otp: otp_record.otp}
      expect(response).to have_http_status(:ok)

      json_response = JSON.parse(response.body)
      expect(json_response).to include("message")


      expect(json_response["message"]).to eql("OTP is valid")
    end
  end
end
