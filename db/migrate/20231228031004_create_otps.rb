class CreateOtps < ActiveRecord::Migration[7.0]
  def change
    create_table :otps do |t|
      t.integer 'otp', limit: 6
      t.datetime 'expires_at', null: false

      t.timestamps
    end
  end
end
