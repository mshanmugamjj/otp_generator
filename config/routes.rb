Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  post '/generate_otp', to: 'otps#generate_otp'
  post '/verify_otp', to: 'otps#verify_otp'

  # Defines the root path route ("/")
  # root "articles#index"
end
