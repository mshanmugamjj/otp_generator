class OtpsController < ApplicationController
  before_action :expiry_time, only: :generate_otp
  skip_before_action :verify_authenticity_token

  #generate & save otp
  def generate_otp
    otp = rand(100000..999999)
    Otp.create(otp: otp, expires_at: @expiry_time)
    render json: { otp: otp, expires_at: @expiry_time }, status: :created
  end

  # verify otp
  def verify_otp
    otp_record = Otp.find_by(otp: params[:otp])
    if otp_record && !otp_record.expired?
      render json: { message: 'OTP is valid' }, status: :ok
    elsif otp_record
      render json: { message: 'OTP is Expired' }, status: 400
    else
      render json: { message: 'OTP is Invalid' }, status: 400
    end
  end

  private

  def expiry_time
    @expiry_time = Time.now + 3.minutes
  end
end
